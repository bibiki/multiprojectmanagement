package multi.project.management.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import multi.project.management.domain.Project;
import multi.project.management.temporary.DBVerificationUtility;

public class ProjectDAO {

	private Statement s;
	private PreparedStatement preparedStatement;
	
	public void addProject(Project p, Connection conn){
		try{
			preparedStatement = conn.prepareStatement("INSERT INTO projects (label, path) VALUES(?, ?)");
			preparedStatement.setString(1, p.getLabel());
			preparedStatement.setString(2, p.getPath());
			preparedStatement.executeUpdate();
		} catch(SQLException se){
			se.printStackTrace();
		} finally{
			try{
				preparedStatement.close();
			} catch(SQLException see){
				see.printStackTrace();
			}
		}
	}
	
	public void updateProject(Project p, Connection conn){
		try{
			preparedStatement = conn.prepareStatement("update projects set label = ?, path = ? where id = ?");
			preparedStatement.setString(1, p.getLabel());
			preparedStatement.setString(2, p.getPath());
			preparedStatement.setInt(3, p.getId());
			preparedStatement.executeUpdate();
		} catch(SQLException se){
			se.printStackTrace();
		}
	}
	
	public List<Project> getAllProjects(Connection conn){
		List<Project> projects = new ArrayList<Project>();
		try{
			preparedStatement = conn.prepareStatement("SELECT * FROM projects");
			ResultSet rs = preparedStatement.executeQuery();
			while(rs.next()){
				System.out.println("inside getAllProjects: " + rs.getString(2) + " " + rs.getString(3));
				Project p = new Project(rs.getString(3), rs.getString(2));
				projects.add(p);
			}
		} catch(SQLException se){
			se.printStackTrace();
			return null;
		}
		return projects;
	}
	
	public Project getProjectByPath(String path){
		return null;
	}
	
	public void delteProject(Project p, Connection conn){
		try{
			preparedStatement = conn.prepareStatement("delete from projects where id = ?");
			preparedStatement.setInt(1, p.getId());
			preparedStatement.executeUpdate();
		} catch(SQLException se){
			se.printStackTrace();
		}
	}
	
	public void deleteProjectsWithNoDirectory(Connection conn){
		try{
			preparedStatement = conn.prepareStatement("delete from projects where PATH = ? or PATH is null");
			preparedStatement.setString(1, "");
			preparedStatement.executeUpdate();
		} catch(SQLException se){
			se.printStackTrace();
		}
	}
}
