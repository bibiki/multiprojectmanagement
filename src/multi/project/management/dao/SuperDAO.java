package multi.project.management.dao;

import java.sql.*;
import java.util.List;

import multi.project.management.domain.Project;
import multi.project.management.temporary.DBVerificationUtility;

public class SuperDAO {
	
	private String driver = "org.apache.derby.jdbc.EmbeddedDriver";
	// the database name
	private String dbName = "multiProjectManagement";
	// define the Derby connection URL to use
	private String connectionURL = "jdbc:derby:" + dbName + ";create=true";
	private Connection conn = null;
	private String createTableProjects = "CREATE TABLE projects (id INT NOT NULL GENERATED ALWAYS AS IDENTITY, label VARCHAR(25) NOT NULL UNIQUE, path VARCHAR(60) NOT NULL UNIQUE)";
	private Statement statement;
	public SuperDAO(){
		try {
			/*
			 * * Load the Derby driver.* When the embedded Driver is used this
			 * action start the Derby engine.* Catch an error and suggest a
			 * CLASSPATH problem
			 */
			Class.forName(driver);
			System.out.println(driver + " loaded. ");
			conn = DriverManager.getConnection(connectionURL);
			if(!DBVerificationUtility.check4Table("projects", conn)){
				statement = conn.createStatement();
				statement.execute(createTableProjects);
			}
		} catch (ClassNotFoundException e) {
			System.err.print("ClassNotFoundException: ");
			System.err.println(e.getMessage());
			System.out.println("\n    >>> Please check your CLASSPATH variable   <<<\n");
		} catch (SQLException se){
			se.printStackTrace();
		}
	}
	
	public void addProject(Project p){
		ProjectDAO pDao = new ProjectDAO();
		pDao.addProject(p, conn);
	}
	
	public void updateProject(Project p){
		ProjectDAO pDao = new ProjectDAO();
		pDao.updateProject(p, conn);
	}
	
	public void deleteProject(Project p){
		ProjectDAO pDao = new ProjectDAO();
		pDao.delteProject(p, conn);
	}
	
	public List<Project> getAllProjects(){
		return new ProjectDAO().getAllProjects(conn);
	}
	
	public void deleteProjectsWithNoDirectory(){
		new ProjectDAO().deleteProjectsWithNoDirectory(conn);
	}
	
	public static void main(String[] args){
		SuperDAO sDao = new SuperDAO();
		Project p = new Project("somePath", "someLabel");
		p.setId(1);
		p.setLabel("My new label");
		sDao.deleteProject(p);
	}
}
