package multi.project.management.domain;

public class Project {

	public int id;
	public String path;
	public String label;
	
	public Project(String path, String label){
		this.path = path;
		this.label = label;
	}
	
	public void setPath(String path){
		this.path = path;
	}
	
	public void setLabel(String label){
		this.label = label;
	}
	
	public String getPath(){
		return this.path;
	}
	
	public String getLabel(){
		return this.label;
	}
	
	public void setId(int id){
		this.id = id;
	}
	
	public int getId(){
		return this.id;
	}
	
	public String toString(){
		return getLabel();
	}
	
	public boolean equals(Project p){
		return this.id == p.getId();
	}
}
