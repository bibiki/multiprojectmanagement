package multi.project.management.statics;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class Cmd {

	
	public static String performCommand(String command, String path) throws Exception{
		String rez = "";
		StringTokenizer tokenizer = new StringTokenizer(command, " ");
		String[] com = new String[tokenizer.countTokens()];
		int i = 0;
		while(tokenizer.hasMoreTokens())
			com[i++] = tokenizer.nextToken();
		ProcessBuilder pb = new ProcessBuilder(com);
		pb.directory(new File(path));
		Process p = pb.start();
		BufferedReader stdInput = new BufferedReader(new InputStreamReader(p.getInputStream()));
		BufferedReader stdError = new BufferedReader(new InputStreamReader(p.getErrorStream()));
		// read the output from the command
		String s = null;
		System.out.println("Here is the standard output of the command:\n");
		while ((s = stdInput.readLine()) != null) {
			rez += s;rez += "\n";
			System.out.println(s);
			System.out.println("---");
		}
		// read any errors from the attempted command
		System.out.println("Here is the standard error of the command (if any):\n");
		while ((s = stdError.readLine()) != null) {
			rez += s;rez += "\n";
			System.out.println(s);
		}
		
		return rez;
	}
}
