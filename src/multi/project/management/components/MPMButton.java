package multi.project.management.components;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JOptionPane;

import multi.project.management.domain.Project;
import multi.project.management.panels.ProjectListPanel;
import multi.project.management.statics.Cmd;

public class MPMButton extends JButton implements ActionListener{

	private final String command;
	private final String mark;
	private final boolean resetProjectFace;
	private final boolean shouldRunOnAllProjects;
	private final boolean shouldProjectBeSelected;
	
	public MPMButton(String face, String command, boolean shouldRunOnAllProjects, String mark, boolean resetProjectFace, boolean shouldProjectBeSelected){
		super(face);
		this.command = command;
		this.shouldRunOnAllProjects = shouldRunOnAllProjects;
		this.mark = mark;
		this.resetProjectFace = resetProjectFace;
		this.shouldProjectBeSelected = shouldProjectBeSelected;
		addActionListener(this);
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		try{
			if(!shouldRunOnAllProjects){
				Project project = (Project)ProjectListPanel.projectsList.getSelectedValue();
				if(null == project && shouldProjectBeSelected){
					JOptionPane.showMessageDialog(null, "No project has been selected. Please select one!");
					return;
				}
				String path = ProjectListPanel.projects.get(project);
				ProjectListPanel.commandOutput.setText(ProjectListPanel.commandOutput.getText() + Cmd.performCommand(command, path) + "\n\n");
				if(resetProjectFace){
					project.setLabel(project.getLabel().replaceAll(mark, ""));
					ProjectListPanel.setUpProjects(false);
				}
				
			}
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
	}
}
