package multi.project.management.components;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Enumeration;

import javax.swing.JButton;

import multi.project.management.domain.Project;
import multi.project.management.panels.ProjectListPanel;
import multi.project.management.statics.Cmd;

public class CheckForOutgoingChangesets extends JButton implements
		ActionListener {

	public CheckForOutgoingChangesets() {
		super("Outgoing??");
		setBackground(Color.RED);
		addActionListener(this);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		Thread t = new Thread(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				Enumeration<Project> projects = ProjectListPanel.projectListModel.elements();
				// List<Project> projectsToPush = new ArrayList<Project>();
				while (projects.hasMoreElements()) {
					Project next = projects.nextElement();
					try {
						String rez = Cmd.performCommand("hg outgoing", ProjectListPanel.projects.get(next));
						if (rez.contains("changeset: ")) {
							next.setLabel(next.getLabel() + " ***");
						}
					} catch (Exception ex) {
						ex.printStackTrace();
					}

				}
				ProjectListPanel.setUpProjects(false);

			}
		});
		t.start();
	}
}
