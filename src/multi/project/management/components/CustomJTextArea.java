package multi.project.management.components;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JTextArea;

import multi.project.management.statics.Cmd;

public class CustomJTextArea extends JTextArea {

	public CustomJTextArea(int rows, int cols){
		super(rows, cols);
		this.setEditable(false);
//		addKeyListener(this);
	}
}
