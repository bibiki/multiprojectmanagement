package multi.project.management.components;

import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JTextField;

public class FocusEnabledJTextField extends JTextField implements FocusListener{

	public FocusEnabledJTextField(int x){
		super(x);
		addFocusListener(this);
	}
	
	@Override
	public void focusGained(FocusEvent e) {
		// TODO Auto-generated method stub
		this.setEnabled(false);
		JFileChooser fileChooser = new JFileChooser();
		int returnValue = fileChooser.showOpenDialog(new JFrame());
		if(returnValue == 0)
			setText(fileChooser.getSelectedFile().getParent());
	}
	
	@Override
	public void focusLost(FocusEvent e) {
		// TODO Auto-generated method stub
		this.setEnabled(true);
	}
}
