package multi.project.management.components;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JButton;



import javax.swing.JOptionPane;

import multi.project.management.domain.Project;
import multi.project.management.panels.ProjectListPanel;

public class AddProjectButton extends JButton implements ActionListener{

	public AddProjectButton(){
		super("Add New Project");
//		setPreferredSize(new Dimension(200, 100));
		addActionListener(this);
	}
	
	public void actionPerformed(ActionEvent e){
		String label = ProjectListPanel.projectLabel.getText();
		String path = ProjectListPanel.projectPath.getText();
		File file = new File(path);
		if(label != null && path != null && !label.equals("") && file.isDirectory()){
			Project p = new Project(path, label);
			ProjectListPanel.superDao.addProject(p);
			ProjectListPanel.projectLabel.setText("");
			ProjectListPanel.projectPath.setText("");
			ProjectListPanel.setUpProjects(false);
		}
		else{
			JOptionPane.showMessageDialog(null, "Duhet label per projektin; path-i duhet te jete direktorium ekzistues");
		}
	}
}
