package multi.project.management.components;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Enumeration;

import javax.swing.JButton;
import javax.swing.SwingUtilities;

import multi.project.management.domain.Project;
import multi.project.management.panels.ProjectListPanel;
import multi.project.management.statics.Cmd;

public class CheckForIncomingChangesets extends JButton implements
		ActionListener {

	public CheckForIncomingChangesets() {
		super("Incoming??");
		setBackground(Color.GREEN);
		addActionListener(this);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		Thread t = new Thread(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				Enumeration<Project> projects = ProjectListPanel.projectListModel.elements();
				try {
					while (projects.hasMoreElements()) {
						Project next = projects.nextElement();
					
						String rez = Cmd.performCommand("hg incoming", ProjectListPanel.projects.get(next));
						if (rez.contains("changeset: ")) {
							next.setLabel(next.getLabel() + " +++");
						}
					}
					SwingUtilities.invokeAndWait(new Runnable(){
						@Override
						public void run(){
							ProjectListPanel.setUpProjects(false);
						}
					});
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		});
		t.start();
	}
}
