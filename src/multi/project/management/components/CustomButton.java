package multi.project.management.components;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JOptionPane;

import multi.project.management.domain.Project;
import multi.project.management.panels.ProjectListPanel;
import multi.project.management.statics.Cmd;

public class CustomButton extends JButton implements ActionListener{

	public CustomButton(String label){
		super(label);
		addActionListener(this);
	}
	public void actionPerformed(ActionEvent e){
		String command = ProjectListPanel.commands.get(e.getActionCommand());
		System.out.println(command);
		Project project = (Project)ProjectListPanel.projectsList.getSelectedValue();
		String path = ProjectListPanel.projects.get(project);
		System.out.println(command + " " + path);
		if(e.getActionCommand().indexOf("Clean") == 0){
			ProjectListPanel.commandOutput.setText("");
			System.out.println("\t cleaning output");
		}
		else{
			try{
				if(project == null){
					JOptionPane.showMessageDialog(null, "Selektoje projektin");
				}
				else{
					ProjectListPanel.commandOutput.setText(ProjectListPanel.commandOutput.getText() + Cmd.performCommand(command, path) + "\n\n");
					if(command != null && command.contains("push")){
						project.setLabel(project.getLabel().replaceAll(" \\*\\*\\*", ""));
						ProjectListPanel.setUpProjects(false);
					}
					else if(command != null && command.contains("pull")){
						project.setLabel(project.getLabel().replaceAll(" \\+\\+\\+", ""));
						ProjectListPanel.setUpProjects(false);
					}
					System.out.println("\texecuting " + command + " " + path);
				}
			}
			catch(Exception exp){
				exp.printStackTrace();
			}
		}
	}
}
