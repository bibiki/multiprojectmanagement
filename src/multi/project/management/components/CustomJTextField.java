package multi.project.management.components;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JTextField;

import multi.project.management.domain.Project;
import multi.project.management.panels.ProjectListPanel;
import multi.project.management.statics.Cmd;

public class CustomJTextField extends JTextField implements KeyListener{

	public CustomJTextField(int cols){
		super(cols);
		addKeyListener(this);
	}
	
	@Override
	public void keyPressed(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		if(e.getKeyCode() == KeyEvent.VK_ENTER){
			String command = ProjectListPanel.commandLine.getText().trim();
			String path = ProjectListPanel.projects.get((Project)ProjectListPanel.projectsList.getSelectedValue());
			if(command.indexOf("hg ") == 0 || command.indexOf("mvn ") == 0){
				try{
					ProjectListPanel.commandOutput.setText(ProjectListPanel.commandOutput.getText() + Cmd.performCommand(command, path) + "\n\n");
					ProjectListPanel.commandLine.setText("");
				}
				catch(Exception exc){
					exc.printStackTrace();
				}
			}
		}
	}
	
	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

}