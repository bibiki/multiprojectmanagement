package multi.project.management.panels;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import multi.project.management.components.AddProjectButton;
import multi.project.management.components.CheckForIncomingChangesets;
import multi.project.management.components.CheckForOutgoingChangesets;
import multi.project.management.components.CustomButton;
import multi.project.management.components.CustomJTextArea;
import multi.project.management.components.CustomJTextField;
import multi.project.management.components.FocusEnabledJTextField;
import multi.project.management.components.MPMButton;
import multi.project.management.dao.SuperDAO;
import multi.project.management.domain.Project;
/**
 * 
 * @author Ngadhnjim Berani <nberani@hotmail.com>
 *
 */
public class ProjectListPanel extends JPanel{

	private JButton mvnCleanInstall;
	private JButton hgPull;
	private JButton hgStatus;
	private JButton hgCommit;
	private JButton hgMerge;
	private JButton hgPush;
	private JButton hgUpdate;
	private JButton cleanOutput;
	private JButton anythingOutgoing;
	private JButton anythingIncoming;
	
	private List<JButton> newButtons = new ArrayList<JButton>();
	public ProjectListPanel(boolean f){
		readButtonsFromXML();
		GridBagLayout gbl = new GridBagLayout();
		setLayout(gbl);
		int i = 0;
		GridBagConstraints gbc;
		for(JButton b : newButtons){
			gbc = new GridBagConstraints();
			gbc.gridx = i++; gbc.gridy = 1;gbc.insets = new Insets(5, 5, 5, 5);
//			mvnCleanInstall = new CustomButton("Mvn Clean Install");
			add(b, gbc);
		}
		
		projectsList = new JList<Project>(projectListModel);//new JList<Object>(projects.keySet().toArray());
		gbc = new GridBagConstraints();
		JScrollPane scrollList = new JScrollPane(projectsList);
		scrollList.setPreferredSize(new Dimension(200, projectListModel.size()*20));
		gbc.gridx = 2; gbc.gridy = 3;gbc.anchor = GridBagConstraints.LINE_START; gbc.gridwidth = GridBagConstraints.REMAINDER;gbc.insets = new Insets(5, 5, 5, 5);
		add(scrollList, gbc);
		
		gbc = new GridBagConstraints();
		gbc.gridx = 1; gbc.gridy = 6;gbc.anchor = GridBagConstraints.LINE_START; gbc.gridwidth = GridBagConstraints.REMAINDER;gbc.insets = new Insets(5, 5, 5, 5);
		commandOutput = new CustomJTextArea(25, 70);
		JScrollPane scroll = new JScrollPane (commandOutput, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		add(scroll, gbc);
		
	}
	
	public static JTextField commandLine;
	public static JTextArea commandOutput;
	
	public static final HashMap<String, String> commands = new HashMap<String, String>();
	public static final HashMap<Project, String> projects = new HashMap<Project, String>();
	public static JList<Project> projectsList;
	public static DefaultListModel<Project> projectListModel;
	
	
	private JLabel pLabel;
	private JLabel pPath;
	public static JTextField projectLabel;
	public static JTextField projectPath;
	
	public static SuperDAO superDao;
	
	static {
		superDao = new SuperDAO();
		projectListModel = new DefaultListModel<>();
		setUpCommands();
		setUpProjects(true);
	}
	
	public ProjectListPanel(){
		GridBagLayout gbl = new GridBagLayout();
		setLayout(gbl);
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.gridx = 1; gbc.gridy = 1;gbc.insets = new Insets(5, 5, 5, 5);
		mvnCleanInstall = new CustomButton("Mvn Clean Install");
		add(mvnCleanInstall, gbc);
		
		gbc = new GridBagConstraints();
		gbc.gridx = 2; gbc.gridy = 1;gbc.insets = new Insets(5, 5, 5, 5);
		hgPull = new CustomButton("Pull");
		add(hgPull, gbc);
		
		gbc = new GridBagConstraints();
		gbc.gridx = 3; gbc.gridy = 1;gbc.insets = new Insets(5, 5, 5, 5);
		hgPush = new CustomButton("Push");
		add(hgPush, gbc);
		
		gbc = new GridBagConstraints();
		gbc.gridx = 4; gbc.gridy = 1;gbc.insets = new Insets(5, 5, 5, 5);
		hgStatus = new CustomButton("Status");
		add(hgStatus, gbc);
		
		gbc = new GridBagConstraints();
		gbc.gridx = 5; gbc.gridy = 1;gbc.insets = new Insets(5, 5, 5, 5);
		hgCommit = new CustomButton("Commit");
		add(hgCommit, gbc);
		
		gbc = new GridBagConstraints();
		gbc.gridx = 6; gbc.gridy = 1;gbc.insets = new Insets(5, 5, 5, 5);
		hgMerge = new CustomButton("Merge");
		add(hgMerge, gbc);
		
		gbc = new GridBagConstraints();
		gbc.gridx = 7; gbc.gridy = 1;gbc.insets = new Insets(5, 5, 5, 5);gbc.anchor = GridBagConstraints.LINE_START;
		hgUpdate = new CustomButton("Update");
		add(hgUpdate, gbc);
		//------------------------------------------------------------------
		gbc = new GridBagConstraints();
		gbc.gridx = 1; gbc.gridy = 2;gbc.insets = new Insets(5, 5, 5, 5);gbc.anchor = GridBagConstraints.LINE_START;gbc.gridwidth = 1;
		pLabel = new JLabel("Project Label");
		add(pLabel, gbc);
		
		gbc = new GridBagConstraints();
		gbc.gridx = 2; gbc.gridy = 2;gbc.insets = new Insets(5, 5, 5, 5);gbc.anchor = GridBagConstraints.LINE_START;gbc.gridwidth = 3;
		projectLabel = new JTextField(15);
		add(projectLabel, gbc);
		
		gbc = new GridBagConstraints();
		gbc.gridx = 5; gbc.gridy = 2;gbc.insets = new Insets(5, 5, 5, 5);gbc.anchor = GridBagConstraints.LINE_START;gbc.gridwidth = 1;
		pPath = new JLabel("Project Path");
		add(pPath, gbc);
		
		gbc = new GridBagConstraints();
		gbc.gridx = 6; gbc.gridy = 2;gbc.insets = new Insets(5, 5, 5, 5);gbc.anchor = GridBagConstraints.LINE_START;gbc.gridwidth = 2;
		projectPath = new FocusEnabledJTextField(15);
		add(projectPath, gbc);

		gbc = new GridBagConstraints();
		gbc.gridx = 8; gbc.gridy = 2;gbc.anchor = GridBagConstraints.LINE_START;gbc.gridwidth = GridBagConstraints.REMAINDER;gbc.insets = new Insets(5, 5, 5, 5);
		add(new AddProjectButton(), gbc);
		
		//------------------------------------------------------------------
		gbc = new GridBagConstraints();
		gbc.gridx = 1; gbc.gridy = 3;gbc.anchor = GridBagConstraints.NORTHWEST; gbc.gridwidth = GridBagConstraints.REMAINDER;gbc.insets = new Insets(5, 5, 5, 5);
		add(new JLabel("Current Projects"), gbc);
		
		projectsList = new JList<Project>(projectListModel);//new JList<Object>(projects.keySet().toArray());
		gbc = new GridBagConstraints();
		JScrollPane scrollList = new JScrollPane(projectsList);
		scrollList.setPreferredSize(new Dimension(200, projectListModel.size()*20));
		gbc.gridx = 2; gbc.gridy = 3;gbc.anchor = GridBagConstraints.LINE_START; gbc.gridwidth = GridBagConstraints.REMAINDER;gbc.insets = new Insets(5, 5, 5, 5);
		add(scrollList, gbc);

		gbc = new GridBagConstraints();
		gbc.gridx = 1; gbc.gridy = 4;gbc.anchor = GridBagConstraints.LINE_START; gbc.gridwidth = GridBagConstraints.REMAINDER;gbc.insets = new Insets(5, 5, 5, 5);
		commandLine = new CustomJTextField(45);
		add(commandLine, gbc);
		
		gbc = new GridBagConstraints();
		gbc.gridx = 8; gbc.gridy = 1;gbc.insets = new Insets(5, 5, 5, 5); gbc.anchor = GridBagConstraints.LINE_START;
		anythingOutgoing = new CheckForOutgoingChangesets();
		add(anythingOutgoing, gbc);
		
		gbc = new GridBagConstraints();
		gbc.gridx = 9; gbc.gridy = 1;gbc.insets = new Insets(5, 5, 5, 5); gbc.anchor = GridBagConstraints.LINE_START;
		anythingIncoming = new CheckForIncomingChangesets();
		add(anythingIncoming, gbc);
		
		gbc = new GridBagConstraints();
		gbc.gridx = 1; gbc.gridy = 5;gbc.insets = new Insets(5, 5, 5, 5); gbc.anchor = GridBagConstraints.LINE_START;
		cleanOutput = new CustomButton("Clean output");
		add(cleanOutput, gbc);
		
		gbc = new GridBagConstraints();
		gbc.gridx = 1; gbc.gridy = 6;gbc.anchor = GridBagConstraints.LINE_START; gbc.gridwidth = GridBagConstraints.REMAINDER;gbc.insets = new Insets(5, 5, 5, 5);
		commandOutput = new CustomJTextArea(25, 70);
		JScrollPane scroll = new JScrollPane (commandOutput, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		add(scroll, gbc);
	}
	
	public static void main(String[] args){
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				// TODO Auto-generated method stub
				JFrame frame = new JFrame();
				frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				frame.add(new ProjectListPanel());
				frame.setVisible(true);
				frame.pack();
			}
		});
	}
	
	private static void setUpCommands(){
		commands.put("Mvn Clean Install", "cmd /c mvn clean install");
		commands.put("Pull", "hg pull");
		commands.put("Push", "hg push");
		commands.put("Status", "hg status");
		commands.put("Commit", "hg commit");
		commands.put("Merge", "hg merge");
		commands.put("Update", "hg update");
	}
	
/**/
	public static void setUpProjects(){
		superDao.deleteProjectsWithNoDirectory();
		List<Project> projs = superDao.getAllProjects();
		projectListModel.removeAllElements();
		for(Project p : projs){
			projectListModel.addElement(p);
			projects.put(p, p.getPath());
		}
	}
	
	public static void setUpProjects(boolean isStarting){
		List<Project> projs = null;
		if(isStarting){
			superDao.deleteProjectsWithNoDirectory();
			projs = superDao.getAllProjects();
		}else
			projs = Collections.list(projectListModel.elements());
		projectListModel.removeAllElements();
		for(Project p : projs){
			projectListModel.addElement(p);
			projects.put(p, p.getPath());
		}
	}
	
	public void readButtonsFromXML(){
		try{
			File buttonsFile = new File("buttons.xml");
			if(null == buttonsFile)
				return;
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = dbFactory.newDocumentBuilder();
			Document doc = builder.parse(buttonsFile);
			doc.getDocumentElement().normalize();
			NodeList buttons = doc.getElementsByTagName("buttons");//get the entire content of xml
			Element b = (Element)buttons.item(0);
			buttons = b.getElementsByTagName("button");//get the inner button elements of buttons element
			for(int i = 0; i < buttons.getLength(); i++){
				Element button = (Element)buttons.item(i);
				JButton mpmButton = new MPMButton(button.getElementsByTagName("face").item(0).getTextContent(),
						button.getElementsByTagName("command").item(0).getTextContent(), 
						"true".equals(button.getElementsByTagName("shouldRunOnAllProjects").item(0).getTextContent()),
						button.getElementsByTagName("mark").item(0).getTextContent(),
						"true".equals(button.getElementsByTagName("resetProjectFace").item(0).getTextContent()),
						"true".equals(button.getElementsByTagName("shouldProjectBeSelected").item(0).getTextContent()));
				newButtons.add(mpmButton);
			}
		}
		catch(ParserConfigurationException pce){
			pce.printStackTrace();
		}
		catch(SAXException saxe){
			saxe.printStackTrace();
		}
		catch(IOException ioe){
			ioe.printStackTrace();
		}
	}
}
